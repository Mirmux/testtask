import logging
import traceback

from rest_framework import status
from rest_framework.response import Response

from core.bot import alert_to_telegram


def custom_exception_handler(exc, context):
    traceback_ = traceback.format_exc()
    logging.warning(traceback_)
    message = str(getattr(exc, 'detail', exc))
    response_data = {
        'message': message
    }
    alert_to_telegram(traceback_, message)
    return Response(
        data=response_data,
        status=getattr(exc, 'status_code', status.HTTP_500_INTERNAL_SERVER_ERROR)
    )
