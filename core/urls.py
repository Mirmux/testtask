from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from core import settings
from core.docs import schema_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('product.urls')),

    # Swagger UI
    path('docs/', schema_view.with_ui(
        'swagger',
        cache_timeout=0,
    )
         )
]
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT
    )
