import telebot

from core.settings import BOT_TOKEN, CHANNEL_ID


def send_alert(text):
    bot = telebot.TeleBot(BOT_TOKEN)
    bot.send_message(CHANNEL_ID, text=f"<code> {text} </code> \n Programmer -> @m_mirfayziyev", parse_mode='html')


def alert_to_telegram(traceback, message):
    if not isinstance(message, list):
        message = message
    text = f'❌Product(Company, Category) Service Exception❌\n\n' \
           f'✍️ Message: 🔴{message}🔴' \
           f'\n\n🔖 TraceBack: {traceback}'
    send_alert(text)
