from collections import defaultdict

from drf_yasg.utils import swagger_auto_schema
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView

from product.models import Product, ProductMaterial, WareHouse
from product.serializers.serializers import ProduceProductSerializer


class ProductList(APIView):

    def get(self, reuqest, *args, **kwargs):
        products = Product.objects.all().values('id', 'name', 'product_code')
        return Response(data=products)


class ProductMaterialView(APIView):

    def get(self, request, *args, **kwargs):
        product_material = Product.objects.all()
        data = [{
            "product_name": product_mat.name,
            "material": product_mat.products.all().values('material__name', 'quantity', 'material__unit')
        }for product_mat in product_material]

        return Response(data=data, status=200)


class WareHouseView(APIView):

    def get(self, request, *args, **kwargs):
        warehouse = WareHouse.objects.all().values('id', 'material__name', 'remainder', 'price')

        return Response(data=warehouse, status=200)


class ProduceProduct(APIView):

    @swagger_auto_schema(request_body=ProduceProductSerializer)
    def post(self, request, *args, **kwargs):
        serializer = ProduceProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        product_id = validated_data.get('product_id')
        quantities = validated_data.get('quantity')
        dict_m = defaultdict(list)

        if len(product_id) != len(quantities):
            raise APIException(detail="You should put same value which quantity and product, Body has to be "
                                      "respectively length with each field!")

        warehouse = WareHouse.objects.all()

        [dict_m[wh.material_id].append({
            'id': wh.id,
            'material_id': wh.material_id,
            'material_name': wh.material.name,
            'unit': wh.material.unit,
            'remainder': wh.remainder,
            'price': wh.price
        }) for wh in warehouse]
        result = []
        try:
            for prod, quantity in zip(product_id, quantities):
                materials = []
                product_materials = ProductMaterial.objects.filter(product_id=prod)
                for p_material in product_materials:
                    waste_quantity = p_material.quantity * quantity
                    qua = dict_m[p_material.material_id]
                    i = 0
                    for wh in qua:
                        len_qua = len(qua)
                        remainder = wh['remainder']
                        wh['remainder'] = wh['remainder'] - waste_quantity

                        if qua[len_qua - 1]['remainder'] == 0:
                            materials.append({
                                "warehouse_id": None,
                                "material_name": wh['material_name'],
                                "quantity": -wh['remainder'],
                                "unit": wh['unit'],
                                "price": None
                            })
                            break

                        if wh['remainder'] < 0 and remainder != 0:
                            materials.append({
                                "warehouse_id": wh['id'],
                                "material_name": wh['material_name'],
                                "quantity": remainder,
                                "unit": wh['unit'],
                                "price": wh['price'],
                                "total price": wh['price'] * quantity
                            })
                            waste_quantity = -wh['remainder']
                            wh['remainder'] = 0
                            try:
                                qua[i + 1]
                            except:
                                materials.append({
                                    "warehouse_id": None,
                                    "material_name": wh['material_name'],
                                    "quantity": waste_quantity,
                                    "unit": wh['unit'],
                                    "price": None
                                })
                            i += 1
                            continue
                        elif wh['remainder'] > 0:
                            materials.append({
                                "warehouse_id": wh['id'],
                                "material_name": wh['material_name'],
                                "quantity": waste_quantity,
                                "unit": wh['unit'],
                                "price": wh['price'],
                                "total price": wh['price'] * quantity
                            })
                            i += 1
                        i += 1
                result.append({
                    "product_name": Product.objects.get(id=prod).name,
                    "product_quantity": quantity,
                    "product_materials": materials
                })
        except Exception as error:
            return Response(data=error)

        return Response(data={"result": result})
