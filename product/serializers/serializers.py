from rest_framework import serializers


class ProduceProductSerializer(serializers.Serializer):
    product_id = serializers.ListSerializer(child=serializers.IntegerField())
    quantity = serializers.ListSerializer(child=serializers.IntegerField())

