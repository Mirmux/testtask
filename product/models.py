from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Product(BaseModel):
    name = models.CharField(max_length=255)
    product_code = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Products'


class Material(BaseModel):
    name = models.CharField(max_length=255)
    unit = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Materials'


class ProductMaterial(BaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='products')
    material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='materials')
    quantity = models.FloatField()

    def __str__(self):
        return self.product.name

    class Meta:
        db_table = 'ProductMaterials'


class WareHouse(BaseModel):
    material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='material_warehouse')
    remainder = models.FloatField()
    price = models.FloatField()

    def __str__(self):
        return self.material.name

    class Meta:
        db_table = 'Warehouse'
