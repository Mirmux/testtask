from django.urls import path

from product.views.views import ProductList, ProduceProduct, ProductMaterialView, WareHouseView

urlpatterns = [
    path('products', ProductList.as_view(),),
    path('produce/products', ProduceProduct.as_view(),),
    path('product/materials', ProductMaterialView.as_view(),),
    path('warehouse', WareHouseView.as_view(),),
]