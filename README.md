**ReadMe**

- Creating virtual environment **venv**

- Installing packages
`pip3 install -r requirements.txt`
  
- Migration 
`python3 manage.py migrate`
  
- Run project
`python3 manage.py runserver 8000`
  
- Docs(Swagger)
`localhost:8000/docs` 

- Backup Database
`testdbbackup.sql`
  
**git branches**

- Production branch
> main

| Method   | URL                                      | Description                              |
| -------- | ---------------------------------------- | ---------------------------------------- |
| `GET`    | `/docs`                                  | Swagger docs      .                      |
| `GET`    | `api/v1/product/materials`               | Get product material.                    |
| `GET`    | `/api/v1/products`                       | Get all product.                         |
| `GET`    | `/api/v1/warehouse`                      | Get Warehouse data.                      |
| `POST`   | `/api/v1/produce/products`               | Calculate product.                       |    

`{
  "product_id": [
    1, 2
  ],
  "quantity": [
    10, 30
  ]
}`
> This is example that are product_id 1 with 10 quantity, and product_id 2 with 30 quantity

- Example environ
- `DEBUG=True`
- `DB_NAME=testdb`
- `DB_USER=postgres`
- `DB_PASS=****`
- `DB_HOST=localhost`
- `DB_PORT=5432`
- `UPLOADING_DIRECTORY=/home/mirmux/Projects/django-rest-framework/upgrade_backend/media`